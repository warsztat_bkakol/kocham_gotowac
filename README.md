
# dzien dobry  
witam w *mojej* kuchni  
  
dzisiaj ~~pieczemy~~ gotujemy **parowki**

![](https://gitlab.com/warsztat_bkakol/kocham_gotowac/-/raw/main/parowka.jpg)
  
do tego potrzebujemy  
  
1. garnek  
2. parowka  
3. woda  
  
jak to mawial dżordż washingtone:  
> parowy sa dobre  
  
uwaga! woda music byc  
- swieza  
- ciepla  
  
zeby zrobic parowy musimy uzyc jezyka `parówthon`  
  
kod gotowania wyglada nastepujaco
```py
import garnek
from jedzenie import paruwa

garnek.gotuj(paruwa)
```

pamietajcie o poprawnej pisowni (`paruwa` a nie `parowa`).
# smacznego.
